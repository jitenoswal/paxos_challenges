import sys
from challenges.challenge_2.common import is_validate_file_int_args, item_file_to_item_dict


def find_triples (arr, target):
    """
        Find triple of 3 numbers from a `arr` which is equal to/or least away from `target`

        `arr` is input array of unique floats in ascending order

        `target` is the non-negative integer

        Returns a triples of 3 elements from arr which satisfies closest/equal-to max target

        Tests:
        >>> find_triples([500, 700, 1000, 1400, 2000, 6000], 2500)
        [500, 700, 1000]
        >>> find_triples([500, 700, 1000, 1400, 2000, 6000], 2300)
        [500, 700, 1000]
        >>> find_triples([500, 700, 1000, 1400, 2000, 6000], 6000)
        [1000, 1400, 2000]
        >>> find_triples([500, 700, 1000, 1400, 2000, 6000], 10000)
        [1400, 2000, 6000]
        >>> find_triples([500, 700, 1000, 1400, 2000, 6000], 1100)
        []
    """

    result = []
    curr_sum = -1
    first, second, third = 0, int(len(arr)/2), (len(arr) - 1)
    while first < second < third:
        temp_sum = arr[first] + arr[third] + arr[second]
        if temp_sum == target:
            result = [arr[first], arr[second], arr[third]]
            break
        elif temp_sum > target:
            if second < third - 1:
                third = third - 1
            else:
                second = second - 1
        else:
            if curr_sum < temp_sum:
                curr_sum = temp_sum
                result = [arr[first], arr[second], arr[third]]
            if second > first + 1:
                first = first + 1
            else:
                second = second + 1
    return result


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: find-triple.py [prices_txt_file] [max_value_of_gift_card]')
        sys.exit(1)

    file_name = sys.argv[1]
    max_target = sys.argv[2]
    triples = None
    if is_validate_file_int_args(file_name, max_target):
        item_dict = item_file_to_item_dict(file_name)

        if item_dict:
            triples = find_triples(list(item_dict.keys()), int(max_target))
            if triples:
                print(str(item_dict[triples[0]]) + ' ' + str(triples[0]) + ', ' + str(item_dict[triples[1]]) + ' ' + str(triples[1]) + ', ' + str(item_dict[triples[2]]) + ' ' + str(triples[2]))
            else:
                print('Not possible')
    else:
        print ('usage: find-triple.py [prices_txt_file] <type:string> [max_value_of_gift_card] <type:integer>')