import sys

from challenges.challenge_2.common import is_validate_file_int_args, item_file_to_item_dict


def find_pair(arr, target):
    """
        Find pair of 2 numbers from a `arr` which is equal to least away from `target`

        `arr` is input array of unique floats in ascending order

        `target` is the non-negative integer

        Returns a pair of 2 elements from arr which satisfies the max target condition

        Tests:
        >>> find_pair([500, 700, 1000, 1400, 2000, 6000], 2500)
        [500, 2000]
        >>> find_pair([500, 700, 1000, 1400, 2000, 6000], 2300)
        [700, 1400]
        >>> find_pair([500, 700, 1000, 1400, 2000, 6000], 10000)
        [2000, 6000]
        >>> find_pair([500, 700, 1000, 1400, 2000, 6000], 1100)
        []
    """

    result = []
    curr_sum = -1
    first, second = 0, len(arr) - 1

    while first < second:
        temp_sum = arr[first] + arr[second]
        if temp_sum == target:
            result = [arr[first], arr[second]]
            break
        elif temp_sum > target:
            second = second - 1
        else:
            if curr_sum < temp_sum:
                curr_sum = temp_sum
                result = [arr[first], arr[second]]
            first = first + 1
    return result


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: find-pair.py [prices_txt_file] [max_value_of_gift_card]')
        sys.exit(1)

    file_name = sys.argv[1]
    max_target = sys.argv[2]
    pair = None
    if is_validate_file_int_args(file_name, max_target):
        item_dict = item_file_to_item_dict(file_name)

        if item_dict:
            pair = find_pair(list(item_dict.keys()), int(max_target))
            if pair:
                print(str(item_dict[pair[0]]) + ' ' + str(pair[0]) + ', ' + str(item_dict[pair[1]]) + ' ' + str(pair[1]))
            else:
                print('Not possible')
    else:
        print ('usage: find-pair.py [prices_txt_file] <type:string> [max_value_of_gift_card] <type:integer>')