import sys
from collections import OrderedDict


def is_validate_file_int_args(file_name, val):
    """
            Validates arg inputs filename <string> & value <integer>

            `file_name` is input filename of pricelist file

            `val` is the non-negative integer

            Returns a bool `true` if valid and `false` if not valid inputs
    """
    try:
        f = open(file_name)
        s = f.readline()
        i = int(val)
    except IOError as e:
        print('Error: ' + str(e))
        return False
    except ValueError:
        print("Error: No valid integer. Please provide non-negative integer")
        return False
    except:
        print("Error: Unexpected error:", sys.exc_info()[0])
        return False
    return True


def is_valid_int(val):
    """
            Validates val is non-negative integer

            `val` is input string

            Returns a bool `true` if valid and `false` if not valid inputs
    """
    try:
        i = int(val)
    except ValueError:
        return False
    return True


def item_file_to_item_dict (file_name):
    """
            Function to convert price list item file to dictionary of value: item

            `file_name` is input filename of price list file which needs to be converted to dictionary

            Returns `OrderedDict of price list items` if valid file contents else will return `None`
    """
    dict = OrderedDict()
    with open(file_name) as fp:
        for line in fp:
            parsed = line.strip().split(', ')
            if is_valid_int(parsed[1]):
                dict[int(parsed[1])] = parsed[0]
            else:
                print('Error: item_file contains price value which is non-integer. Please change/update the input file')
                return None
    return dict

