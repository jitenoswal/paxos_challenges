import sys


def recur_binary_str_permutations(input_str, l, r, output):
    """
    Non recursive approach to listing all possible permutation for missing characters in binary `input_str`
    represented as X with 0 and 1 simultaneously to get possible outcomes.

    `input_str` is input string of binary characters with missing chars replaced with X.

    Prints each possible binary string output to console by replacing X with 0 and 1 (like asked in challenge)

    Tests:
    >>> recur_binary_str_permutations('X0',0,len('X0'),'')
    00
    10
    >>> recur_binary_str_permutations('10X10X0',0,len('10X10X0'),'')
    1001000
    1001010
    1011000
    1011010
    >>> recur_binary_str_permutations('0X',0,len('0X'),'')
    00
    01
    >>> recur_binary_str_permutations('00',0,len('00'),'')
    00
    >>> recur_binary_str_permutations('11',0,len('11'),'')
    11
    >>> recur_binary_str_permutations('',0,len(''),'')
    Traceback (most recent call last):
        ...
    ValueError: Error: [input_str] cannot be null. Please provide non-null [input_str]
    """

    if input_str == '':
        raise ValueError("Error: [input_str] cannot be null. Please provide non-null [input_str]")

    if l == r & len(output) == r:
        print (output)
        return

    for i in range(l, r):
        if input_str[i] == '0':
            recur_binary_str_permutations(input_str, i + 1, r, output + '0')
        elif input_str[i] == '1':
            recur_binary_str_permutations(input_str, i + 1, r, output + '1')
        elif input_str[i] == 'X':
            recur_binary_str_permutations(input_str, i + 1, r, output + '0')
            recur_binary_str_permutations(input_str, i + 1, r, output + '1')
        i += 1


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: recur-find-binary-permutations.py [input_str]')
        sys.exit(1)

    try:
        recur_binary_str_permutations(sys.argv[1], 0, len(sys.argv[1]), '')
    except ValueError as e:
        print(str(e))
    except:
        print("Error: Unexpected error:", str(sys.exc_info()[0]))