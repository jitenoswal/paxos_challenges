import sys


def binary_str_permutations(input_str):
    """
    Non recursive approach to listing all possible permutation for missing characters in binary `input_str`
    represented as X with 0 and 1 simultaneously to get possible outcomes.

    `input_str` is input string of binary characters with missing chars replaced with X.

    Prints each possible binary string output to console by replacing X with 0 and 1 (like asked in challenge)

    Tests:
    >>> binary_str_permutations('X')
    0
    1
    >>> binary_str_permutations('X0')
    00
    10
    >>> binary_str_permutations('10X10X0')
    1001000
    1001010
    1011000
    1011010
    >>> binary_str_permutations('0X')
    00
    01
    >>> binary_str_permutations('00')
    00
    >>> binary_str_permutations('11')
    11
    >>> binary_str_permutations('')
    Traceback (most recent call last):
        ...
    ValueError: Error: [input_str] cannot be null. Please provide non-null [input_str]
    """

    if input_str == '':
        raise ValueError("Error: [input_str] cannot be null. Please provide non-null [input_str]")

    string_stack = [input_str]

    while string_stack:
        string = string_stack.pop(0)
        i = 0
        cnt = 0
        final_output = ''
        for char in string:
            if char == '0' or char == '1':
                final_output = final_output + char
                cnt += 1
            elif char == 'X':
                string_stack.append(string[:i] + '0' + string[i + 1:])
                string_stack.append(string[:i] + '1' + string[i + 1:])
                break
            if cnt == len(input_str):
                print (final_output)
                break
            i += 1


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: find-binary-permutations.py [input_str]')
        sys.exit(1)

    try:
        binary_str_permutations(sys.argv[1])
    except ValueError as e:
        print(str(e))
    except:
        print("Error: Unexpected error:", str(sys.exc_info()[0]))