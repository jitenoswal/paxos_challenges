from distutils.core import setup

setup(
    name='paxos_challenge',
    version='0.9dev',
    packages=['challenges',
              'challenges.challenge_2',
              'challenges.challenge_3'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
    author='Jiten Oswal',
    author_email='jiten.p.oswal@gmail.com',
    url='https://github.com/jitenoswal',
)