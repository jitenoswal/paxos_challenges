# Table of Contents
1. [Introduction](README.md#introduction)
2. [Setup Instructions](README.md#setup-instructions)
3. [All Challenges Solutions](README.md#all-challenges-solutions)
4. [Challenge 1](README.md#challenge-1)
5. [Challenge 2](README.md#challenge-2)
6. [Optional Challenge 2](README.md#optional-challenge-2)
7. [Challenge 3](README.md#challenge-3)


# Introduction

This git consist of solutions to all the 3 Challenges including the optional Challenge #2. 


# Setup Instructions:

Please run following to setup the packages correctly (from paxos_challenges as base):

    pip install .
   
This setups up the project using setup.py. It describes all of the metadata about the project. Metadata fields like name, version, and packages which are required also some optional fields like license, author, url, author_email. 
    
# All Challenges Solutions:

#####****Note: Run each from their specific base directories.


# Challenge 1

#####Link: https://bitbucket.org/jitenoswal/challenge_1_api
		
		
# Challenge 2

Program to find 2 max valued items from prices text file to be gifted to 2 friends.

#####Time Complexity: O(n)

#####Command to Run: 

		python find-pair.py <price-list_text_file_sorted> <max_value_of_gift_card_to_use>
		
#####Example Command to Run: 

		python find-pair.py prices.txt 2500
		
#####Output:

		Candy Bar 500, Earmuffs 2000
		
#####Run Test:

		python -m doctest -v find-pair.py

# Optional Challenge 2

Program to find 3 max valued items from prices text file to be gifted to 3 friends.

#####Time Complexity: O(n)

#####Command to Run: 

		python find-triples.py <price-list_text_file_sorted> <max_value_of_gift_card_to_use>

#####Example Command to Run: 

		python find-triples.py prices.txt 10000

#####Output:

		Headphones 1400, Earmuffs 2000, Bluetooth Stereo 6000

#####Run Test:

		python -m doctest -v find-triples.py

# Challenge 3

Program to list all permutations of binary string replacing 'X' with '0' and '1' consecutively.

#####Time Complexity: O(n*2^m)

#####Command to Run: 


		python find-binary-permutations.py <string_of_binary_chars>

#####Example Command to Run: 

		python find-binary-permutations.py 10X101

#####Output:

		100101
		101101

#####Run Test:

		 python -m doctest -v find-binary-permutations.py

####[Ignore/Additional] Challenge 3 with Recursion

Recursive approach to find all permutations of binary string replacing 'X' with '0' and '1'

#####Command to Run: 

		python recur-find-binary-permutations.py <string_of_binary_chars>

#####Example Command to Run:

		python recur-find-binary-permutations.py 10XX

#####Output:

		1000
		1001
		1010
		1011

#####Run Test:

		python -m doctest -v recur-find-binary-permutations.py 
